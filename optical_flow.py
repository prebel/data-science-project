#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  9 21:17:53 2018

@author: dineshvashisht
"""

import numpy as np
import cv2
import imutils
import time
cap = cv2.VideoCapture(0)

st_feat = dict(maxCorners=1000,qualityLevel=0.1,minDistance=7,blockSize=7)
lk_params = dict(winSize=(15,15),maxLevel=2,criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
color=[0,255,0]
ret,old_frame = cap.read()
#old_frame = imutils.resize(old_frame,width=500)
old_gray= cv2.cvtColor(old_frame,cv2.COLOR_BGR2GRAY)

po = cv2.goodFeaturesToTrack(old_gray,mask=None,**st_feat)




mask=np.zeros_like(old_frame)


while True:
    (grabbed,frame)=cap.read()
    #frame = imutils.resize(frame,width=500)
    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    p1,st,err = cv2.calcOpticalFlowPyrLK(old_gray,gray,po,None,**lk_params)

    good_new = p1[st==1]
    good_old = po[st==1]
    for i,(old,new) in enumerate(zip(good_old,good_new)):
        nx,ny = new.ravel()
        ox,oy = old.ravel()
        mask = cv2.line(mask,(ox,oy),(nx,ny),[0,255,0],2)
        frame=cv2.circle(frame,(ox,oy),5,[0,255,0],-1)
    frame=cv2.add(frame,mask)
    cv2.imshow('frame',frame)
    val = cv2.waitKey(1) & 0xff
    if(val=='q'):
        break
    old_gray= gray.copy()
    po = good_new.reshape(-1,1,2)
    mask=np.zeros_like(old_frame)
cv2.destroyAllWindows()
cap.release()




    
    