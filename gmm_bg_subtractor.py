#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 23:33:25 2018

@author: dineshvashisht
"""

import numpy as np
import cv2


cap = cv2.VideoCapture(0)
fgbg =cv2.bgsegm.createBackgroundSubtractorMOG(history=5, nmixtures=5)
while True:
    ret ,frame = cap.read()
    grey = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    grey = cv2.GaussianBlur(grey,(11,11),0)
    fgmask = fgbg.apply(grey)
    dilation = cv2.dilate(fgmask,np.ones((20,20),np.uint(8)))
    dilation = cv2.dilate(dilation, None, iterations=2)
    cnts = cv2.findContours(dilation.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    for c in cnts[1]:
        if cv2.contourArea(c)<10000:
            continue
        else:
            (x, y, w, h) = cv2.boundingRect(c)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
    
    cv2.imshow('fgmask',frame)
    cv2.waitKey(1)
cap.release()
cv2.destroyAllWindows()