import numpy as np
import cv2
import imutils
import time
import sys
import datetime
import stepmtr as SP
from multiprocessing import Process, Queue

from picamera.array import PiRGBArray
from picamera import PiCamera


# Bring the camera back into position
offset_movement = 0
steps = 10
delay = 5

def reset_camera_position():
    global offset_movement
    global steps
    global delay

    print offset_movement
    if offset_movement > 0:
        while offset_movement is not 0:
            SP.backward(int(delay) / 1000.0, int(steps))
            offset_movement -= steps
            
    elif offset_movement > 0:
        while offset_movement is not 0:
            SP.forward(int(delay) / 1000.0, int(steps))
            offset_movement += steps
            
            

        
def stepper_driver(queue, x):
    sys.stdin.close()
    global steps
    global delay
    global offset_movement
    
    try:
        while(1):
            turn = queue.get()

            if turn is 1:
                SP.forward(int(delay) / 1000.0, int(steps))
                offset_movement += steps
               
                print('left')
                
            elif turn is 2:
                SP.backward(int(delay) / 1000.0, int(steps))
                offset_movement -= steps
                
                print('right')
    except (KeyboardInterrupt, SystemExit):    
        return

driver_queue = Queue()
SM_driver = Process(target=stepper_driver, args=(driver_queue, 1))
SM_driver.daemon = True
SM_driver.start()

def area(window):
    return window[2] * window[3]


def combine_recs(recs):
    x_max, y_max, w_max, h_max = tuple(recs[0])
    xd_max = x_max + w_max
    yd_max = y_max + h_max

    for r in recs[1:]:
        (x, y, w, h) = tuple(r)
        xd = x + w
        yd = y + h

        x_max = min(x_max, x)
        y_max = min(y_max, y)
        xd_max = max(xd_max, xd)
        yd_max = max(yd_max, yd)

    w_max = xd_max - x_max
    h_max = yd_max - y_max

    return [x_max, y_max, w_max, h_max] 


def check_turn(track_window, red_border):
    x, _, w, _ = track_window
    x_i, _, w_i, _ = red_border

    if x <= x_i:
        return 1
    elif x + w >= x_i + w_i:
        return 2 
    else:
        return 0


camera = PiCamera()
camera.resolution=(640,480)
camera.framerate = 30
rawCapture = PiRGBArray(camera,size=(640,480))
row_len = 640
col_len = 480
frame_size = (row_len, col_len)
time.sleep(0.1)
fgbg =cv2.bgsegm.createBackgroundSubtractorMOG(history=5, nmixtures=5)
i=0
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    i=i+1
    frame1 = frame.array
    rotation = cv2.getRotationMatrix2D((row_len/2, col_len/2), 180, 1)

    frame = cv2.warpAffine(frame1, rotation, frame_size)

    
    grey = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    grey = cv2.GaussianBlur(grey,(11,11),0)
    fgmask1 = fgbg.apply(grey)

    rawCapture.truncate(0)
    if(i>20):
        break

    
    cv2.imshow('img2', frame)
    cv2.waitKey(1)



left_limit = int(row_len * .2)
right_limit = row_len - 2 * left_limit

red_border = (left_limit, 0, right_limit, col_len)

track_window = [0, 0, 0, 0]
frame_track=0

start_time = time.time()
i=0
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # take first frame of the video
    frame1 = frame.array

    rotation = cv2.getRotationMatrix2D((row_len/2, col_len/2), 180, 1)

    frame = cv2.warpAffine(frame1, rotation, frame_size)
    
    i=i+1
    print(i)

    grey = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    grey = cv2.GaussianBlur(grey,(11,11),0)
    fgmask = fgbg.apply(grey)
    dilation = cv2.dilate(fgmask,np.ones((20,20)))
    #dilation = cv2.dilate(dilation, None, iterations=2)
    cnts = cv2.findContours(dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    recs = []
    for c in cnts[1]:
        if cv2.contourArea(c) < 5000:
            continue
        else:
            rec = list(cv2.boundingRect(c))

            recs.append(rec)
            

    if len(recs):
        rec = combine_recs(recs)

        if (area(track_window) < area(rec)):
            track_window = rec
            frame_track=frame

    x, y, w, h = tuple(track_window)
    cv2.rectangle(dilation, (x, y), (x + w, y + h), (255, 255, 255), 3)

    cv2.imshow('img2', dilation)
    cv2.waitKey(1)

    end_time = time.time()
    rawCapture.truncate(0)
    if(end_time - start_time > 5):
        break
    
    
    
track_window = tuple(track_window)
(r, c, h, w) = track_window


# set up the ROI for tracking
roi = frame_track[c:c+w,r:r+h]

hsv_roi =  cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)

mask = cv2.inRange(hsv_roi, np.array((0., 60.,32.)), np.array((180.,255.,255.)))

roi_hist = cv2.calcHist([hsv_roi],[0,1],mask,[12,8],[0,180,0,255])

cv2.normalize(roi_hist,roi_hist,0,255,cv2.NORM_MINMAX)
# Setup the termination criteria, either 10 iteration or move by atleast 1 pt
term_crit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )

# output file for captured frames
filename = "{}-{}.avi".format(datetime.date.today(), np.random.randint(10000))
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter(filename,fourcc, 10, frame_size)

print "filename: {}".format(filename)
print "frame dimentions: {}".format(frame_size)

# Turn LED ON
SP.led_on()

try:
    for frame1 in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        frame1 = frame1.array

        rotation = cv2.getRotationMatrix2D((row_len/2, col_len/2), 180, 1)

        frame = cv2.warpAffine(frame1, rotation, frame_size)

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        dst = cv2.calcBackProject([hsv],[0,1],roi_hist,[0,180,0,255],1)
        
        #disc = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
        #cv2.filter2D(dst,-1,disc,dst)
        ret, dst = cv2.threshold(dst,0,255,cv2.THRESH_OTSU)
        #dst = cv2.dilate(dst,np.ones((10,10),np.uint(8)))
            # apply meanshift to get the new location
        ret, track_window = cv2.CamShift(dst, track_window, term_crit)
            # Draw it on image

        pts = cv2.boxPoints(ret)
        pts = np.int0(pts)

        turn = check_turn(track_window, red_border)

        out.write(frame)
        img2 = cv2.polylines(frame,[pts],True, 255,2)
        
        # Add rotation code here: 1 left. 2 right.
        if turn is 1:
            cv2.putText(img2, "left", (39, 39), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), 1);
            driver_queue.put(turn)

        elif turn is 2:
            cv2.putText(img2,"right", (39, 39), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), 1);
            driver_queue.put(turn)
        
        x, y, w, h = red_border
        cv2.rectangle(img2, (x, y), (x + w, y + h), (0, 0, 255), 1)
        
        cv2.imshow('img2', img2)
        rawCapture.truncate(0)
        k = cv2.waitKey(1) & 0xff
        if k == 27:
            break
        
except (KeyboardInterrupt, SystemExit):    
    pass



SM_driver.join()
reset_camera_position()
SP.led_off()
cv2.destroyAllWindows()
out.release()
