#!/usr/bin/env

import cv2
import numpy as np
import imutils
import time

# Globals
subtractor = None
first_frame = None
buff = None
i = 0


# Generator that perpetually sends video frames until Keyboard Interupt
# Error Handling to be added
def video_capture():
    video = cv2.VideoCapture(0)

    try:
        while(True):
            (_, frame) = video.read()

            yield frame
    except (KeyboardInterrupt, SystemExit):
        video.release()
        cv2.destroyAllWindows()
        return


# Function to preprocess images
def preprocess(frame):
    frame = imutils.resize(frame, width=500)

    grey = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    grey = cv2.GaussianBlur(grey, (11, 11), 0)

    return grey


# Initialise background subtraction algorithms
def init_subtractors(background_method):
    if background_method is MOG_subtraction:
        global subtractor
        subtractor = cv2.bgsegm.createBackgroundSubtractorMOG()

    elif background_method is background_subtraction:
        frame = video_capture().next();
        grey = preprocess(frame)
        outframe = background_subtraction(grey)


# Background subtraction after modelling the background as a Mixture
# of Gaussians
def MOG_subtraction(grey):
    global subtractor
    fgmask = subtractor.apply(grey)

    return fgmask


# Compute background subtracted frames for fields provided
# global will be replaced by class attributes
def background_subtraction(grey):
    global i
    global buff
    global first_frame

    i = (i + 1) % 2
    
    if i == 1:
        first_frame = grey
    else:
        frame_delta = cv2.absdiff(first_frame, grey)
        buff = cv2.threshold(frame_delta, 20, 255, cv2.THRESH_BINARY)[1]

    return buff


# Function to perform object deduction using background subtraction
def runner(background_method):
    # Initialising first frame
    init_subtractors(background_method)

    for frame in video_capture():
        frame = preprocess(frame)

        outframe = background_method(frame)

        cv2.imshow('object detection', outframe)
        cv2.waitKey(2)


def main():
    runner(MOG_subtraction)

if __name__ == '__main__':
    main()
